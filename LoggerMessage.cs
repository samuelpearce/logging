﻿// Logging Class
// To use, call Logging.Logger.Log(message)
// Alternatively add 'using Logging' to the top of the file and call Logger.Log(Message)
// The form will auto display on first log message and in debug mode
// When in release mode, the form will not display (Hold CTRL+SHIFT on program start up to display)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace uk.me.samuel.logging
{       
    public partial class LoggerMessage : Form
    {
        // Allow cross thread calls
        public delegate void SetTextDelegate(System.Windows.Forms.Control ctrl, string text);

        public LoggerMessage()
        {
            InitializeComponent();            
        }
        

        public void SetTextAndShow(String Message)
        {
            this.Show();
            SetTextUpdate(textBox1, Message);
        }
        
        // Allows for cross thread calls
        public static void SetTextUpdate(System.Windows.Forms.Control ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, text };
                ctrl.Invoke(new SetTextDelegate(SetTextUpdate), params_list);
            }
            else
            {
                ctrl.Text = text;
            }
        }

        /// <summary>
        /// This method is called by the textbox to allow the textbox to scroll to the latest message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogBox_TextChanged(object sender, EventArgs e)
        {
            // Get sender
            TextBox S = sender as TextBox;
            S.SelectionStart = S.Text.Length;
            S.ScrollToCaret();
        }

        private void LoggerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }  
}
