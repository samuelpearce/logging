﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;

namespace uk.me.samuel.logging
{
    /// <summary>
    /// To Log file, access this static class
    /// </summary>
    public static class Logging
    {
        public static event NewMessageHandler NewMessage;
        public delegate void NewMessageHandler(String Message);

        private static LoggerForm lf = new LoggerForm();

        /// <summary>
        /// To Log to message box
        /// </summary>
        /// <param name="message">The message you want to show (no need to add time)</param>
        [DebuggerHidden]
        public static void Log(String message)
        {
            try
            {
                // If run in debug mode, show the form
                #if DEBUG
                    ShowForm(lf);
                #endif

                // if release, you need to hold down CTRL+SHIFT+D to show the debugger
                if ((Control.ModifierKeys & Keys.Shift) != 0)
                    ShowForm(lf);
            }
            catch { }

            StackFrame frame = new StackFrame(1);
            var method = frame.GetMethod();
            var name = method.Name;
            var mth = new StackTrace().GetFrame(1).GetMethod().ReflectedType.Name;
            
            // If someone has subscribed to the event
            if (NewMessage != null)
            {                
                // Fire event
                StringBuilder sb = new StringBuilder();
                sb.Append(DateTime.Now.ToString());
                sb.Append(" - ");
                sb.Append(name);
                sb.Append(" - ");
                sb.Append(message);
                sb.AppendLine();
                sb.AppendLine();
                sb.Append(method);
                NewMessage(sb.ToString());


            }

        }

        public static void ShowForm(System.Windows.Forms.Form Form)
        {
            if (Form.InvokeRequired)
            {
                Form.Invoke(new ShowDelegate(ShowForm), Form);
            }
            else
            {
                Form.Show();
            }
        }

        public delegate void ShowDelegate(System.Windows.Forms.Form ctrl);
    }

}
