﻿// Logging Class
// To use, call Logging.Logger.Log(message)
// Alternatively add 'using Logging' to the top of the file and call Logger.Log(Message)
// The form will auto display on first log message and in debug mode
// When in release mode, the form will not display (Hold CTRL+SHIFT on program start up to display)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace uk.me.samuel.logging
{       
    public partial class LoggerForm : Form
    {
        // Allow cross thread calls
        public delegate void SetTextDelegate(System.Windows.Forms.ListBox ctrl, string text);

        private LoggerMessage lm = new LoggerMessage();

        public LoggerForm()
        {
            InitializeComponent();            
        }

        private void LoggerForm_Load(object sender, EventArgs e)
        {
            // Subscribe to the Logger Event
            Logging.NewMessage += new Logging.NewMessageHandler(Logger_NewMessage);
            
            // Tell the Windows Form to go to the top right hand corner of the primary screen
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, 0);
            // Full height
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        private void Logger_NewMessage(String Message)
        {
            // Update Textbox (using seperate method to allow for cross thread calls
            SetTextUpdate(listBox, Message);
        }

        // Allows for cross thread calls
        private static void SetTextUpdate(System.Windows.Forms.ListBox ctrl, string text)
        {
            if (ctrl.InvokeRequired)
            {
                object[] params_list = new object[] { ctrl, text };
                ctrl.Invoke(new SetTextDelegate(SetTextUpdate), params_list);
            }
            else
            {
                ctrl.Items.Add(text);

                if (ctrl.Items.Count != 0)
                {
                    ctrl.SelectedIndex = ctrl.Items.Count - 1;
                }
            }
        }

        /// <summary>
        /// This method is called by the textbox to allow the textbox to scroll to the latest message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogBox_TextChanged(object sender, EventArgs e)
        {
            // Get sender
            TextBox S = sender as TextBox;
            S.SelectionStart = S.Text.Length;
            S.ScrollToCaret();
        }

        private void LoggerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void listBox_Click(object sender, EventArgs e)
        {
            lm.SetTextAndShow((string)listBox.SelectedItem);
        }
    }   
}
